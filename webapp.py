from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/')
def index():
    return "<h1>IaaS SRE AWX POC Demo v1</h1>"

if __name__ == '__main__':
    app.run(host='0.0.0.0',port='5000',debug=True)
